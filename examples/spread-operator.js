let backends = ['NodeJS', 'Meteor', 'MongoDB'];
let frontends = ['HTML', 'CSS', 'Javascript'];
let fullstack = [...frontends, ...backends];
console.log(fullstack);  // ['HTML', 'CSS', 'Javascript', 'NodeJS', 'Meteor', 'MongoDB'];

let obj1 = {
  x: 1,
  y: 2
};
let obj2 = {
  x: 2,
  z: 4
};
let obj3 = {...obj1, ...obj2};
console.log(obj3);  // {x: 2, y: 2, z: 4}