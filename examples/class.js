class Person {
  constructor(name = 'Tien', age = 22) {
    this.name = name;
    this.age = age;
  }

  describePersonalInfo() {
    console.log(`Hello. My name is ${this.name}. I am ${this.age} year(s) old.`);
  }
}

let newPerson1 = new Person('Minh', 30);
newPerson1.describePersonalInfo();  // "Hello. My name is Minh. I am 30 year(s) old."

//Inheritance
class Employee extends Person {
  constructor(name, age, job) {
    super(name, age);
    this.job = job;
  }

  describePersonalInfo() {
    return this.job ? console.log(`Hello. My name is ${this.name}. I am ${this.age} year(s) old. I am working as a(n) ${this.job}`) : super.describePersonalInfo();
  }

}

let newPerson2 = new Employee('Minh', 40 ,'Developer');
newPerson2.describePersonalInfo();  // "Hello. My name is Minh. I am 40 year(s) old. I am working as a(n) Developer."
let newPerson3 = new Employee('Nguyen', 35);
newPerson3.describePersonalInfo(); // "Hello. My name is Nguyen. I am 35 year(s) old."