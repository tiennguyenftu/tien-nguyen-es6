# Lesson Learnt

## **let** keyword

**let** allows you to declare variables that are limited in scope to the block, statement, or expression on which it is used. **var** keyword defines a variable globally, or locally to an entire function regardless of block scope.
For example:
```
var x = 1;
if (x) {
  var x = 2;
}
console.log(x);  // 2

let x = 1;
if (x) {
  let x = 2;
}
console.log(x);  // 1
```
```
for (var i = 0; i++; i < 5) {
  
} 
console.log(i);  // 5
//Vs
for (let j = 0; j++; j < 5) {
  
} 
console.log(j);  // Error: i is not defined
```

## **const** keyword
**const** allows you to declare constants. Constants are block-scoped, much like variables defined using the **let** statement. The value of a constant cannot change through re-assignment, and it can't be redeclared.
For example:
```
var x = 5;
x = 4;
console.log(x);  // 4

let y = 5;
y = 4;
console.log(x);  // 4

const z = 5;
z = 4;
console.log(x);  // TypeError: Assignment to constant variable
```
## Spread Operator
The **spread syntax** allows an expression to be expanded in places where multiple arguments (for function calls) or multiple elements (for array literals) or multiple variables (for destructuring assignment) are expected.
```
let backends = ['NodeJS', 'Meteor', 'MongoDB'];
let frontends = ['HTML', 'CSS', 'Javascript'];
let fullstack = [...frontends, ...backends];
console.log(fullstack);  // ['HTML', 'CSS', 'Javascript', 'NodeJS', 'Meteor', 'MongoDB'];
```

```
let obj1 = {
  x: 1,
  y: 2
};
let obj2 = {
  x: 2,
  z: 4
}
let obj3 = {...obj1, ...obj2};
console.log(obj3);  // {x: 2, y: 2, z: 4}
```
## Template strings
Template literals are string literals allowing embedded expressions. You can use multi-line strings and string interpolation features with them.

```
let name = 'Tien Nguyen';
let greeting1 = 'Hello ' + 'Tien Nguyen' + '!';
console.log(greeting1);  // "Hello Tien Nguyen!"
let greeting2 = `Hello ${name}!`;
console.log(greeting2);  // "Hello Tien Nguyen!"
```

## Default function parameters
Default function parameters allow formal parameters to be initialized with default values if no value or undefined is passed.

```
function add(x = 1, y = 2) {
  return x + y;
}
console.log(add());  // 3
console.log(add(3));  // 5
console.log(add(4, 5));  // 9
```
## Object literals
A JavaScript **object literal** is a comma-separated list of name-value pairs wrapped in curly braces. Object literals encapsulate data, enclosing it in a tidy package. This minimizes the use of global variables which can cause problems when combining code.
For examples, ES6 added **repeat** method for enhancing object literals
```
let cat = {
  meow() {
    console.log('meow'.repeat(5));
  }
};
cat.meow();  // "meowmeowmeowmeowmeow"
```

## Arrow function
An **arrow function** expression has a shorter syntax than a function expression and does not bind its own **this**, **arguments**, **super**, or **new.target**. These function expressions are best suited for non-method functions, and they cannot be used as constructors.

```
const names = ['Nguyen', 'Tien', 'Minh'];
const greetings = names.map(name => {
  console.log(`Hello ${name}!`);
});
// "Hello Nguyen!"
// "Hello Tien!"
// "Hello Minh!"
```
```
let person = {
  first: 'Tien',
  actions: ['bike', 'hike', 'ski', 'surf'],
  printActions: function() {
    this.actions.forEach(function() {
      let str = `${this.first} likes to ${action}`;
      console.log(str);
    });
  } 
};
person.printActions();  // Cannot read property 'first' of undefined

let person = {
  first: 'Tien',
  actions: ['bike', 'hike', 'ski', 'surf'],
  printActions() {
    this.actions.forEach((action) => {
      let str = `${this.first} likes to ${action}`;
      console.log(str);
    });
  } 
};
person.printActions();
// "Tien likes to bike"
// "Tien likes to hike"
// "Tien likes to ski"
// "Tien likes to surf"
```

## Destructuring assignment
The **destructuring assignment** syntax is a JavaScript expression that makes it possible to unpack values from arrays, or properties from objects, into distinct variables.

```
let [firstName, lastName, age, gender] = ['Tien', 'Nguyen', 22, 'male'];
console.log(firstName);  // "Tien"
console.log(age);  // 22
```
```
let obj = {
  x: 1,
  y: 2,
  z: 3
};
let {x, y, z} = obj;
console.log(x, y, z);  // 1 2 3
```
```
let info = {
  firstName: 'Tien',
  lastName: 'Nguyen',
  age: 22
}
function greeting({firstName, lastName}) {
  return `Hello ${firstName} ${lastName}!`;
}
console.log(greeting(info));  // "Hello Tien Nguyen!"
```

## Generators
For example:
```
function* idMaker() {
  let index = 0;
  while(true)
    yield index++;
}

let gen = idMaker();

console.log(gen.next().value); // 0
console.log(gen.next().value); // 1
console.log(gen.next().value); // 2
```

## ES6 Class
JavaScript classes introduced in ECMAScript 2015 are primarily syntactical sugar over JavaScript's existing prototype-based inheritance. The class syntax is **not** introducing a new object-oriented inheritance model to JavaScript. JavaScript classes provide a much simpler and clearer syntax to create objects and deal with inheritance.

```
class Person {
  constructor(name = 'Tien', age = 22) {
    this.name = name;
    this.age = age;
  }
  
  describePersonalInfo() {
    console.log(`Hello. My name is ${this.name}. I am ${this.age} year(s) old.`);
  }
}

let newPerson1 = new Person('Minh', 30);
newPerson1.describePersonalInfo();  // "Hello. My name is Minh. I am 30 year(s) old."

//Inheritance
class Employee extends Person {
  constructor(name, age, job) {
    super(name, age);
    this.job = job;
  }
  
  describePersonalInfo() {
    return this.job ? console.log(`Hello. My name is ${this.name}. I am ${this.age} year(s) old. I am working as a(n) ${this.job}`) : super.describePersonalInfo();
  }
  
}

let newPerson2 = new Employee('Minh', 40 ,'Developer');
newPerson2.describePersonalInfo();  // "Hello. My name is Minh. I am 40 year(s) old. I am working as a(n) Developer."
let newPerson3 = new Employee('Nguyen', 35);
newPerson3.describePersonalInfo(); // "Hello. My name is Nguyen. I am 35 year(s) old."
```
