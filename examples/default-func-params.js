function add(x = 1, y = 2) {
  return x + y;
}
console.log(add());  // 3
console.log(add(3));  // 5
console.log(add(4, 5));  // 9