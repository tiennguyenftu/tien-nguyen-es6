var x = 1;
if (x) {
  var x = 2;
}
console.log(x);  // 2

let y = 1;
if (y) {
  let y = 2;
}
console.log(y);  // 1
for (var i = 0; i++; i < 5) {

}
console.log(i);  // 5
//Vs
for (let j = 0; j++; j < 5) {

}
// console.log(j);  // Error: i is not defined