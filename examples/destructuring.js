let [firstName, lastName, age, gender] = ['Tien', 'Nguyen', 22, 'male'];
console.log(firstName);  // "Tien"
console.log(age);  // 22
let obj = {
  x: 1,
  y: 2,
  z: 3
};
let {x, y, z} = obj;
console.log(x, y, z);  // 1 2 3
let info = {
  firstName: 'Tien',
  lastName: 'Nguyen',
  age: 22
};
function greeting({firstName, lastName}) {
  return `Hello ${firstName} ${lastName}!`;
}
console.log(greeting(info));  // "Hello Tien Nguyen!"