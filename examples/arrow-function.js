const names = ['Nguyen', 'Tien', 'Minh'];
const greetings = names.map(name => {
  console.log(`Hello ${name}!`);
});
// "Hello Nguyen!"
// "Hello Tien!"
// "Hello Minh!"
let person = {
  first: 'Tien',
  actions: ['bike', 'hike', 'ski', 'surf'],
  printActions: function() {
    this.actions.forEach(function() {
      let str = `${this.first} likes to ${action}`;
      console.log(str);
    });
  }
};
person.printActions();  // Cannot read property 'first' of undefined

let person2 = {
  first: 'Tien',
  actions: ['bike', 'hike', 'ski', 'surf'],
  printActions() {
    this.actions.forEach((action) => {
      let str = `${this.first} likes to ${action}`;
      console.log(str);
    });
  }
};
person2.printActions();
// "Tien likes to bike"
// "Tien likes to hike"
// "Tien likes to ski"
// "Tien likes to surf"