let cat = {
  meow() {
    console.log('meow'.repeat(5));
  }
};
cat.meow();  // "meowmeowmeowmeowmeow"